package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadSteps {
	public ChromeDriver driver;
	
	@Given("Launch the Browser")
	public void launchTheBrowser() {
		
        System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver/chromedriver.exe");
		
		driver=new ChromeDriver();
	}
	
	@Given("Maximise the Browser")
	public void maximiseTheBrowser() {
		driver.manage().window().maximize();
	}

	@Given("Set the Timesout")
	public void setTheTimesout() {
	    driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
	}

	@Given("Load the URL")
	public void loadTheURL() {
		driver.get("http://leaftaps.com/opentaps");
	}

	@Given("Enter the Username")
	public void enterTheUsername() {
		driver.findElementById("username").sendKeys("DemosalesManager");
		
	}

	@Given("Enter the Password")
	public void enterThePassword() {
		driver.findElementById("password").sendKeys("crmsfa");
	}

	@When("I click on the Login Button")
	public void iClickOnTheLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@When("click on the CRM\\/SFA link")
	public void clickOnTheCRMSFALink() {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@When("Click on the create leads")
	public void clickOnTheCreateLeads() {
		driver.findElementByLinkText("Create Lead").click();;
	}

	@When("Enter the Company name as (.*)")
	public void enterTheCompanyName(String Cname) {
		driver.findElementById("createLeadForm_companyName").sendKeys(Cname);
		
		
	}

	@When("Enter the First Name as (.*)")
	public void enterTheFirstName(String Fname) {
		driver.findElementById("createLeadForm_firstName").sendKeys(Fname);
	}

	@When("Enter the Last Name as (.*)")
	public void enterTheLastName(String Lname) {
		driver.findElementById("createLeadForm_lastName").sendKeys(Lname);
	}

	@When("Click on the Create Lead button")
	public void clickOnTheCreateLeadButton() {
		driver.findElementByName("submitButton").click();
	}

	@Then("Verify the lead is created")
	public void verifyTheLeadIsCreated() {
	    String Title = driver.getTitle();
	    if(Title.equals("View Lead | opentaps CRM"))
	    {
	    System.out.println("Verified");
	}
	    else
	    {
	    	System.out.println("No");
	    }


}
}
