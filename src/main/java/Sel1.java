import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testleaf.leaftaps.seleniumbase.ProjectSpecificMethods;

public class Sel1 {
	public class login extends ProjectSpecificMethods {
		
	}
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver/chromedriver.exe");
		
		ChromeDriver driver=new ChromeDriver();
		
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemosalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Cognizant");
		driver.findElementById("createLeadForm_firstName").sendKeys("Yanadi");
		driver.findElementById("createLeadForm_lastName").sendKeys("Vishnu Priya");
		
		WebElement ele = driver.findElementById("createLeadForm_dataSourceId");
		Select dd = new Select(ele);
		dd.selectByVisibleText("Employee");
		
		WebElement ele1 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dd1 = new Select(ele1);
	    List<WebElement> Res = dd1.getOptions();
	    int size = Res.size();
	    dd1.selectByIndex(size-2);	
	   driver.findElementByName("submitButton").click();
	   String text = driver.findElementById("viewLead_companyName_sp").getText();
	   String Result = text.replaceAll("\\D", "");
	   System.out.println(Result);
	   
	   driver.findElementByXPath("//a[@class='subMenuButtonDangerous']").click();
	   Thread.sleep(3000);
	   driver.findElementByXPath("//a[text()='Find Leads']").click();
	   driver.findElementByXPath("(//div[@class='x-form-item x-tab-item'])[1]//input").sendKeys(Result);
	   driver.findElementByXPath("//button[text()='Find Leads']").click();
	   Thread.sleep(5000);
	   String text2 = driver.findElementByXPath("//div[@class='x-paging-info']").getText();
	   String text3= "No records to display";
	   if(text2.equals(text3))
	   {
		   System.out.println("Yes");
	   }
	   
	}
	

} 
