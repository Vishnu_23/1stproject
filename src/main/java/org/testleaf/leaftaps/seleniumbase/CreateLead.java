package org.testleaf.leaftaps.seleniumbase;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CreateLead extends ProjectSpecificMethods {
	@BeforeClass
	
	public void setData() {
		excelFileName = " CreateLead";
	}
	@Test(dataProvider= "fetchdata")
	
	public void runcreatelead(String CName, String fName, String lName) {
	

	driver.findElementByLinkText("Create Lead").click();
	driver.findElementById("createLeadForm_companyName").sendKeys(CName);
	driver.findElementById("createLeadForm_firstName").sendKeys(fName);
	driver.findElementById("createLeadForm_lastName").sendKeys(lName);
	driver.findElementByName("submitButton").click();
}
	
	@DataProvider(name = "fetchdata")
	public String[][] getdata() {
	
	String [][] data =  new String [2][3];
	data[0][0]="TestLeaf";
	data[0][1]="Haja";
	data[0][2]="M";
	
	data[1][0]="TestLeaf";
	data[1][1]="Balaji";
	data[1][2]="P";
	
	return data;
	
}
}
