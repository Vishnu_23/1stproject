package org.testleaf.leaftaps.seleniumbase;

import java.io.IOException;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import utils.ReadExcelData;

public class ProjectSpecificMethods {

	public  ChromeDriver driver;
	public String excelFileName;
	@Parameters({"url", "uName","uPswd"})
	@BeforeMethod
	
	public void login(String url, String uName, String uPswd) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver/chromedriver.exe");
		driver=new ChromeDriver();
		
		driver.get(url);
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys(uName);
		driver.findElementById("password").sendKeys(uPswd);
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();

	}
	
	@DataProvider(name = "fetchdata")
	public void read() throws IOException {
	   ReadExcelData.ReadExcel(excelFileName);
   }
     
	
	
}

