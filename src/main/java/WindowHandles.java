import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandles {
	
	public static void main(String[] args) throws IOException {
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver/chromedriver.exe");
			
			ChromeDriver driver=new ChromeDriver();
			driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			
			driver.findElementByLinkText("Contact Us").click();
			System.out.println(driver.getTitle());
			
			//String WindowHnadle = driver.getWindowHandle();
			Set<String> allwindows = driver.getWindowHandles();
			
			
			List<String> listwindows = new ArrayList<String>(allwindows);
			
			driver.switchTo().window(listwindows.get(1));
			System.out.println(driver.getTitle());
			
			File src = driver.getScreenshotAs(OutputType.FILE);
			File target = new File("./snaps/Img001.jpg");
			FileUtils.copyFile(src, target);
		}
	
}