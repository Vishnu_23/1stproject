import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Drag {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver/chromedriver.exe");
		
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/drag.html");
		WebElement drag = driver.findElementByXPath("//div[@id='draggable']");
	
		
		Actions builder =  new Actions(driver);
		builder.dragAndDropBy(drag, 100, 100).perform();
	}
		
	}

