import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Select3 {

	public static void main(String[] args) {
System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver/chromedriver.exe");
		
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/selectable.html");
		WebElement one = driver.findElementByXPath("//li[text()='Item 1']");
		WebElement two = driver.findElementByXPath("//li[text()='Item 2']");
		WebElement three = driver.findElementByXPath("//li[text()='Item 3']");
		
		Actions builder =  new Actions(driver);
		builder.clickAndHold(one).clickAndHold(two).clickAndHold(three).release().perform();
		
		
	}
}
