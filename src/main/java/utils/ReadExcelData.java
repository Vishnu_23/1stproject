package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class ReadExcelData{
	
	@Test

public static  String[][] ReadExcel(String wBookName) throws IOException {
	
	//G0 to file location and open the file
	XSSFWorkbook wBook = new XSSFWorkbook("./Data/"+wBookName+".xlsx");
	
	//Navigate to particular sheet
	
	XSSFSheet Sheet = wBook.getSheet("Sheet1");
	
	//Find no. of rows in sheet
	
	int rowcount = Sheet.getLastRowNum();
	
	//Find no. of columns in sheet
	
	int Cellcount = Sheet.getRow(0).getLastCellNum();
	
	String data[] [] = new String [rowcount][Cellcount];
	//Read data inside each cell
	
	for(int i=1; i<= rowcount;i++)
	{
		XSSFRow row = Sheet.getRow(i);
		
		for(int j=0; j< Cellcount;j++)
		{
			XSSFCell cell = row.getCell(j);
			data[i-1][j] = cell.getStringCellValue();
		
		}
	}
	wBook.close();
	return data;	

}
}
